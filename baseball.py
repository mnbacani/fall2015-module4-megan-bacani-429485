import urllib2
import sys
import re
name = re.compile(r'\b[A-Z]\w*\b \b[A-Z]\w*\b')
nums = re.compile(r'\d+')
if len(sys.argv) != 2:
	print "Wrong number of arguments"
else:
	data = open(sys.argv[1])
	players = {}
	for line in data:
		nameFound = name.match(line)
		numsFound = re.findall(r'\d+', line)
		if nameFound is not None:
			players[nameFound.group()] = [0.0,0.0]
		
	data2 = open(sys.argv[1])
	for line in data2:
        	nameFound = name.match(line)
        	numsFound = re.findall(r'\d+', line)
        	if nameFound is not None:
                	players[nameFound.group()] = [(players.get(nameFound.group()))[0] + float(numsFound[0]), (players.get(nameFound.group()))[1] + float(numsFound[1])]          


	ans = {}	
	for player, hits in sorted(players.items()): 
		ans[player] = (round((hits[1])/(hits[0]), 3))

	for player in sorted(ans, key = ans.get, reverse = True):
        	print "%s: %0.3f" % (player, players.get(player)[1]/players.get(player)[0])

